#!/bin/bash

source /var/lib/rundeck/data/openrc

KO=0
echo "[INFO] Checking Ironic nodes' status in nova database "

# remove .cern.ch and lowercase all instances passed
NORMALIZED_INSTANCES=`echo $RD_OPTION_INSTANCES | sed 's/.cern.ch//g' | awk '{print tolower($0)}'`
for INSTANCE in $NORMALIZED_INSTANCES
do
    INSTANCE_ID=$(openstack server list --all-projects --name $INSTANCE -n -c ID -f value)

    if [[ -z "${INSTANCE_ID}" ]]; then
      echo "[INFO] No project found for instance $INSTANCE. This not an Ironic-managed node!. Moving on to the next node..."
      continue
    else
      echo "[INFO] Ironic instance $INSTANCE ($INSTANCE_ID) was found. Checking status..."
    fi

    echo "[INFO] Checking nova status for instance '$INSTANCE'..."
    NOVA_STATUS=$(openstack server list --all-projects --name $INSTANCE -n -c Status -f value)
    echo "[INFO] Current status of '$INSTANCE' in nova's database is '$NOVA_STATUS'."

    if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
      if [[ $NOVA_STATUS = "ACTIVE" ]]; then
        echo "[INFO] '$INSTANCE_ID' is already ACTIVE in nova's databases. Moving on to the next node..."
        continue
      else
        echo "[INFO] Trying to turn on '$INSTANCE_ID'..."
        EXIT_CODE=$(openstack server start $INSTANCE_ID)
        COUNTER=30
        until [[ COUNTER -eq "0" ]]; do
          echo "[INFO] Checking nova status for instance '$INSTANCE_ID'..."
          NOVA_STATUS=$(openstack server list --all-projects --name $INSTANCE -n -c Status -f value)
          echo "[INFO] Current Status is $NOVA_STATUS"
          if [[ $NOVA_STATUS = "ACTIVE" ]]; then
            echo "[INFO] '$INSTANCE_ID' successfully turned on"
            break 2
          fi
          COUNTER=$((COUNTER-1))
          sleep 10
        done
        ((KO++))
      fi

      if [[ $EXIT_CODE -ne 0 ]]; then
        ((KO++))
      else
        echo "[INFO] Node was turned ON successfully!"
      fi

    else
      echo "[DRYRUN] Turning ON ironic instance '$INSTANCE'..."
    fi

done

if [[ $K0 -ne 0 ]]; then
  echo "[ERROR] Turning ON through OpenStack failed for some Ironic nodes. Please check the logs"
  exit 1
fi
