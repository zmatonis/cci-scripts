#!/bin/bash

source /var/lib/rundeck/data/openrc

OK=0
KO=0

#only execute this once since it takes reallyyy long
export OS_REGION_NAME=cern
NOVA_SERVICE_LIST_CERN="$(nova service-list --binary nova-compute)"
export OS_REGION_NAME=batch
NOVA_SERVICE_LIST_BATCH="$(nova service-list --binary nova-compute)"
export OS_REGION_NAME=point8
NOVA_SERVICE_LIST_POINT8="$(nova service-list --binary nova-compute)"

echo "[INFO] Trying to stop the nova-compute service on the compute nodes $RD_OPTION_HOSTS..."
echo $RD_OPTION_HOSTS | wassh -f - -l root --ssh -q 'systemctl stop openstack-nova-compute ; systemctl disable openstack-nova-compute ; puppet agent --disable "Removing node from the cloud"'

echo "[INFO] Trying to remove the following compute nodes from Nova: $RD_OPTION_HOSTS..."

if [ -n "$NOVA_SERVICE_LIST_CERN" ] || [ -n "$NOVA_SERVICE_LIST_BATCH" ]; then
  for HOST in $RD_OPTION_HOSTS
    do
      # Gettting cell info
      HOST_LOWER=${HOST,,} #lower case hostname

      TEMP_ID1=$(echo "$NOVA_SERVICE_LIST_CERN" | awk "/$HOST_LOWER/" | awk '{print $2}')
      TEMP_ID2=$(echo "$NOVA_SERVICE_LIST_BATCH" | awk "/$HOST_LOWER/" | awk '{print $2}')
      TEMP_ID3=$(echo "$NOVA_SERVICE_LIST_POINT8" | awk "/$HOST_LOWER/" | awk '{print $2}')

      if [ -n "$TEMP_ID1" ]; then
        REGION_NAME=cern
        HOST_ID=$TEMP_ID1
      elif [ -n "$TEMP_ID2" ]; then
        REGION_NAME=batch
        HOST_ID=$TEMP_ID2
      elif [ -n "$TEMP_ID3" ]; then
        REGION_NAME=point8
        HOST_ID=$TEMP_ID3	
      else
        echo "[ERROR] Not found. Has $HOST been deleted already?"
        ((KO++))
        continue
      fi

      export OS_REGION_NAME=$REGION_NAME
      echo "[INFO] Removing compute node $HOST from Nova..."
      if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
        echo "[INFO] Executing: nova service-delete $HOST_ID..."

        nova service-delete "$HOST_ID"
        if [ $? -eq 0 ]; then
          echo "[INFO] $HOST successfully deleted"
          ((OK++))
        else
          sleep 1 #log messages sync
          echo "[ERROR] Failed to delete compute node on from Nova"
          ((KO++))
        fi
      else
        echo "[DRYRUN] Would've removed compute node $HOST from Nova"
        ((OK++))
      fi
      echo ""
    done
else
  echo "[ERROR] Something went wrong executing nova service-list. Please check with the OpenStack admins."
  exit 2
fi

#Summary
printf "\n:TOTAL HOSTS:SUCCESS:ERROR\n[${RD_OPTION_BEHAVIOUR^^}] SUMMARY SERVICE DELETE:     $(($OK + $KO)):   $OK:  $KO" | column  -t -s ':'
echo ""

if [ $KO -gt 0 ]; then
  echo "[ERROR] Not possible to remove some compute nodes from Nova. Please check logs."
  exit 2
fi
