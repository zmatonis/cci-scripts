#!/bin/bash

source /var/lib/rundeck/data/openrc

OK=0
KO=0

#only execute this once since it takes really long
export OS_REGION_NAME=cern
NEUTRON_AGENT_LIST_CERN="$(neutron agent-list)"
export OS_REGION_NAME=batch
NEUTRON_AGENT_LIST_BATCH="$(neutron agent-list)"
export OS_REGION_NAME=point8
NEUTRON_AGENT_LIST_POINT8="$(neutron agent-list)"

echo "[INFO] Trying to remove the following linuxbrige-agent from Neutron: $RD_OPTION_HOSTS..."

if [ -n "$NEUTRON_AGENT_LIST_CERN" ] || [ -n "$NEUTRON_AGENT_LIST_BATCH" ]; then
  for HOST in $RD_OPTION_HOSTS
    do
      # Gettting cell info
      HOST_LOWER=${HOST,,} #lower case hostname

      TEMP_ID1=$(echo "$NEUTRON_AGENT_LIST_CERN" | awk "/$HOST_LOWER/ && /neutron-linuxbridge-agent/" | awk '{print $2}')
      TEMP_ID2=$(echo "$NEUTRON_AGENT_LIST_BATCH" | awk "/$HOST_LOWER/ && /neutron-linuxbridge-agent/" | awk '{print $2}')
      TEMP_ID3=$(echo "$NEUTRON_AGENT_LIST_POINT8" | awk "/$HOST_LOWER/ && /neutron-linuxbridge-agent/" | awk '{print $2}')

      if [ -n "$TEMP_ID1" ]; then
        REGION_NAME=cern
        HOST_ID=$TEMP_ID1
      elif [ -n "$TEMP_ID2" ]; then
        REGION_NAME=batch
        HOST_ID=$TEMP_ID2
      elif [ -n "$TEMP_ID3" ]; then
        REGION_NAME=point8
        HOST_ID=$TEMP_ID3
      else
        echo "[WARNING] Not found. $HOST is not a neutron host"
        ((KO++))
        continue
      fi

      export OS_REGION_NAME=$REGION_NAME
      echo "[INFO] Removing linuxbridge-agent $HOST from Neutron..."
      if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
        echo "[INFO] Executing: neutron agent-delete $HOST_ID"
        neutron agent-delete "$HOST_ID"
        if [ $? -eq 0 ]; then
          echo "[INFO] $HOST successfully deleted"
          ((OK++))
        else
          sleep 1 #log messages sync
          echo "[ERROR] Failed to delete linuxbrige-agent from Neutron"
          ((KO++))
        fi
      else
        echo "[DRYRUN][INFO] Would've removed linuxbrige-agent $HOST from Neutron"
        ((OK++))
      fi
      echo ""
    done
else
  echo "[ERROR] Something went wrong executing neutron agent-list. Please check with the OpenStack admins."
  exit 2
fi

#Summary
printf "\n:TOTAL HOSTS:SUCCESS:ERROR\n[${RD_OPTION_BEHAVIOUR^^}] SUMMARY SERVICE DELETE:     $(($OK + $KO)):   $OK:  $KO" | column  -t -s ':'
echo ""
