#!/bin/bash

source /var/lib/rundeck/data/openrc

OK=0
KO=0

#only execute this once since it takes really long
export OS_REGION_NAME=cern
NEUTRON_AGENT_LIST_CERN="$(neutron agent-list)"
export OS_REGION_NAME=batch
NEUTRON_AGENT_LIST_BATCH="$(neutron agent-list)"
export OS_REGION_NAME=point8
NEUTRON_AGENT_LIST_POINT8="$(neutron agent-list)"

if [[ ${RD_OPTION_REFERENCE} == "" ]]; then
  REASON_MESSAGE="${RD_OPTION_STATUS^}d by $RD_JOB_USER_NAME using Rundeck ($RD_JOB_EXECID)"
else
  REASON_MESSAGE="[$RD_OPTION_REFERENCE] ${RD_OPTION_STATUS^}d by $RD_JOB_USER_NAME using Rundeck ($RD_JOB_EXECID)"
fi

if [ -n "$NEUTRON_AGENT_LIST_CERN" ] || [ -n "$NEUTRON_AGENT_LIST_BATCH" ]; then
  for HOST in $RD_OPTION_HOSTS
    do
      # Gettting cell info
      HOST_LOWER=${HOST,,} #lower case hostname

      TEMP_ID1=$(echo "$NEUTRON_AGENT_LIST_CERN" | awk "/$HOST_LOWER/ && /neutron-linuxbridge-agent/" | awk '{print $2}')
      TEMP_ID2=$(echo "$NEUTRON_AGENT_LIST_BATCH" | awk "/$HOST_LOWER/ && /neutron-linuxbridge-agent/" | awk '{print $2}')
      TEMP_ID3=$(echo "$NEUTRON_AGENT_LIST_POINT8" | awk "/$HOST_LOWER/ && /neutron-linuxbridge-agent/" | awk '{print $2}')

      if [ -n "$TEMP_ID1" ]; then
        REGION_NAME=cern
        HOST_ID=$TEMP_ID1
      elif [ -n "$TEMP_ID2" ]; then
        REGION_NAME=batch
        HOST_ID=$TEMP_ID2
      elif [ -n "$TEMP_ID3" ]; then
        REGION_NAME=point8
        HOST_ID=$TEMP_ID3
      else
        echo "[WARNING] Not found. $HOST is not a neutron host"
        ((KO++))
        continue
      fi

      export OS_REGION_NAME=$REGION_NAME
      echo "[INFO] Trying to $RD_OPTION_STATUS neutron-linuxbridge-agent on $HOST_ID..."
      if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
        if [ ${RD_OPTION_STATUS} == 'disable' ]; then
          neutron agent-update $HOST_ID --admin-state-down
        else
          neutron agent-update $HOST_ID --admin-state-up
        fi
        if [ $? -eq 0 ]; then
          echo "[INFO] neutron-linuxbridge-agent successfully ${RD_OPTION_STATUS}d on $HOST."
          ((OK++))
        else
          sleep 1 # output messages order
          echo "[ERROR] Failed to "$RD_OPTION_STATUS" neutron-linuxbridge-agent on $HOST."
          ((KO++))
        fi
      else
        echo "[DRYRUN][INFO] neutron agent-$RD_OPTION_STATUS $HOST_INFO neutron-linuxbridge-agent"
        ((OK++))
      fi
      echo ""
    done
else
  echo "[ERROR] Something went wrong executing neutron agent-list. Please check with the OpenStack admins."
  exit 2
fi

#Summary
printf "\n:TOTAL HOSTS:SUCCESS:ERROR\n[${RD_OPTION_BEHAVIOUR^^}] SUMMARY NEUTRON ${RD_OPTION_STATUS^^}:     $((OK + KO)):   $OK:  $KO" | column  -t -s ':'
echo ""
