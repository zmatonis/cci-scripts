#!/bin/bash

JOBID="$1"
USERNAME="$2"
JOBURL="$3"
STATUS='@option.status@'
RD_OPTION_PUPPET_LOCK='@option.puppet_lock@'
REASON='@option.message@'
EXECUSER=$(whoami)

function is_puppet_enabled {
  if [[ -f $RD_OPTION_PUPPET_LOCK ]]; then
    return 1
  fi
}

if [[ $EXECUSER != 'root' ]]; then
  echo >&2 "This job cannot be executed on Rundeck server"
  exit 1
fi

if [ ! -z "$REASON" ]; then
  REASON+=" ($JOBURL)"
else
  REASON="${STATUS^}d by $USERNAME using Rundeck ($JOBURL)"
fi

sleep 1 # sync
echo Running puppet agent --$STATUS \"$REASON\" on $HOSTNAME...
PUPPET_EXIT=0

if [ $STATUS == 'disable' ]; then
  if is_puppet_enabled; then
    puppet agent -v --disable "$REASON"
    PUPPET_EXIT=$?
  else
    echo "Puppet already disabled. Nothing to do."
  fi

# puppet enable
else
  if ! is_puppet_enabled; then
    puppet agent -v --enable
    PUPPET_EXIT=$?
  else
    echo "Puppet seems to be enabled already. Nothing to do."
  fi
fi

exit $PUPPET_EXIT
