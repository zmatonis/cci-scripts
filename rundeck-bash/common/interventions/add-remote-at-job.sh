#!/bin/bash

#only if mode 'perform' and command not empty
if [ "${RD_OPTION_BEHAVIOUR}" == "perform" ] && [ "${RD_OPTION_COMMAND}" != "- None -" ]; then
  DATE_HOUR=(${RD_OPTION_DATE})
  if [ "${RD_OPTION_EXPRESSION}" == "True" ]; then
    COMMAND="echo -n "${RD_OPTION_COMMAND}" '#managed_by_rundeck' | at -M ${RD_OPTION_DATE}"
  else
    DATE=${DATE_HOUR[0]}
    HOUR=${DATE_HOUR[1]}

    # convert date to a valid input for "at" ie. American format + "/"
    IFS='-' read -a myarray <<< $DATE
    DATE="${myarray[1]}/${myarray[0]}/${myarray[2]}"

    # add managed_by_rundeck so we can identify it later on
    COMMAND="echo -n "${RD_OPTION_COMMAND}" '#managed_by_rundeck' | at -M $HOUR $DATE"
  fi

  for HOST in $RD_OPTION_HOSTS
    do
      OUT=$(ssh -q -o StrictHostKeyChecking=no root@$HOST "${COMMAND}" 2>&1)
      JOBID=$(echo $OUT | awk {'print $2'})
      if [  ! -z "$JOBID" ]; then
        if [ "${RD_OPTION_EXPRESSION}" == "True" ]; then
          echo "[INFO] New AT job created. $HOST will execute $RD_OPTION_COMMAND using '${RD_OPTION_DATE}' AT expression."
        else
          echo "[INFO] New AT job created. $HOST will execute $RD_OPTION_COMMAND on ${DATE_HOUR[0]} at $HOUR."
        fi
      else
        echo "[ERROR] Remote AT job could not be created. Kerberos ticket expired?"
        exit 1
      fi
    done
fi
