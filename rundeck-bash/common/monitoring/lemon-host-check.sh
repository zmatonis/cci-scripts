#!/bin/bash

if [ ${RD_OPTION_EXECUTION_MODE} == 'force' ]; then
    echo "[INFO] execution_mode set to 'force', no need to perform the check"
    exit 0
fi
  
OK=0
KO=0
OK_HOSTS=""
KO_HOSTS=""

for HOST in $RD_OPTION_HOSTS
  do
    echo "[INFO] Checking exceptions on host $HOST..."
    ACTIVE_EXCEPTION=$(ssh -q -o StrictHostKeyChecking=no root@$HOST collectdctl listval state=ERROR)
    if [[ $? -eq 0 ]]; then
      if [[ $ACTIVE_EXCEPTION == "" ]]; then
        echo "[INFO] No exceptions found"
        OK_HOSTS="$OK_HOSTS $HOST"
        echo ""
        ((OK++))
      else
        ACTIVE_EXCEPTION=$(echo $ACTIVE_EXCEPTION|tr -d '\n')
        echo "[ERROR] Found exception '$ACTIVE_EXCEPTION'"
        KO_HOSTS="$KO_HOSTS $HOST"
        echo ""
        ((KO++))
      fi
    else
      echo "[ERROR] Could not connect to '$HOST'. Please verify hostname and credentials"
      KO_HOSTS="$KO_HOSTS $HOST"
      echo ""
      ((KO++))
    fi
  done

#Summary
printf "\n:TOTAL HOSTS:SUCCESS:ERROR\n SUMMARY:     $((OK + KO)):   $OK:  $KO" | column  -t -s ':'
echo ""

if [ $KO -ne 0 ]; then
  echo "[ERROR] These hosts returned errors: $KO_HOSTS. Please fix them before running this job again."
  if [ ! -z "$OK_HOSTS" ]; then
    echo "[INFO] You can rerun this job only using the ones that did not return any errors, to do it please change the input host list to:"
    echo "$OK_HOSTS"
  fi
fi
