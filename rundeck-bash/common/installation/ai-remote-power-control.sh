#!/bin/bash

if [ "$RD_OPTION_OPERATION" == "none" ]; then
    echo "[INFO] No IPMI operation introduced. Skipping ai-remote-power-control execution..."
    exit 0
fi

if [ "$RD_OPTION_OPERATION" == "cycle" ]; then
    OPENSTACK_OPERATION="reboot --hard"
elif [ "$RD_OPTION_OPERATION" == "on" ]; then
    OPENSTACK_OPERATION="start"
elif [ "$RD_OPTION_OPERATION" == "off" ]; then
    OPENSTACK_OPERATION="stop"
elif [ "$RD_OPTION_OPERATION" == "soft" ]; then
    OPENSTACK_OPERATION="reboot"
fi

source /var/lib/rundeck/data/openrc

OK=0
KO=0

declare -A HOSTS

# remove .cern.ch and lowercase all hosts passed
NORMALIZED_HOSTS=`echo $RD_OPTION_HOSTS | sed 's/.cern.ch//g' | awk '{print tolower($0)}'`
echo "[INFO] Mapping hosts $NORMALIZED_HOSTS to projects"

for HOST in $NORMALIZED_HOSTS
do
    VM_ID=$(openstack server list --all-projects --name $HOST -n | grep $HOST | awk '{print $2}')

    if [[ -z "${VM_ID}" ]]; then
        PRO_ID="ISN_T_IT_IRONIC" # Default value for non-Ironic nodes
        echo "[INFO] No project found for host $HOST. Not yet Ironic-managed?"
    else
        PRO_ID=$(openstack server show $VM_ID | grep project_id | awk '{print $4}')
        echo "[INFO] Machine '$HOST' is inside '$PRO_ID'"
        echo "[INFO] Adding svcrdeck to project '$PRO_ID'..."
        OS_PROJECT_NAME=admin OS_PROJECT_ID= openstack role add --user svcrdeck --project $PRO_ID Member
    fi

    HOSTS["$PRO_ID"]+=" $HOST"
done

for PRO_ID in ${!HOSTS[@]}
do
    COMPUTE_NODES=${HOSTS["$PRO_ID"]}
    echo "[INFO] Project $PRO_ID - Hosts $COMPUTE_NODES"
    if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then


      if [ "$PRO_ID" == "ISN_T_IT_IRONIC" ]; then

        echo "[INFO] Executing ai-remote-power-control $RD_OPTION_OPERATION on the following hosts: $COMPUTE_NODES ..."
        SUMMARY=$(OS_PROJECT_NAME= OS_PROJECT_ID=$PRO_ID ai-remote-power-control $RD_OPTION_OPERATION $COMPUTE_NODES 2>&1)
        EXIT_CODE=$?
      else
        echo "[INFO] Executing openstack server $OPENSTACK_OPERATION over $COMPUTE_NODES in project $PRO_ID"
        SUMMARY=$(echo "$COMPUTE_NODES" | xargs -n 1 env OS_PROJECT_NAME= OS_PROJECT_ID=$PRO_ID openstack server $OPENSTACK_OPERATION 2>&1)
        EXIT_CODE=$?
      fi

      echo "$SUMMARY"

      HOST_OK=$(echo "$SUMMARY" | grep -wF '| OK |' | awk '{print $2}' | wc -l)

      if [[ $EXIT_CODE -ne 0 ]] && [[ $HOST_OK -eq 0 ]]; then
        ((OK++))
      elif [[ $EXIT_CODE -ne 0 ]] && [[ $HOST_OK -ne 0 ]]; then
        ((KO++))
      fi
    else

      if [ "$PRO_ID" == "ISN_T_IT_IRONIC" ]; then
        echo "[DRYRUN] Executing OS_PROJECT_NAME= OS_PROJECT_ID=$PRO_ID ai-remote-power-control $RD_OPTION_OPERATION $COMPUTE_NODES 2>&1)"
        OS_PROJECT_NAME= OS_PROJECT_ID=$PRO_ID ai-remote-power-control --dryrun $RD_OPTION_OPERATION $RD_OPTION_HOSTS
      else
        echo "[DRYRUN] Executing openstack server $OPENSTACK_OPERATION $COMPUTE_NODES 2>&1)"
      fi

      ((OK++))
    fi

    if [[ "${PRO_ID}" != "ISN_T_IT_IRONIC" ]]; then
      echo "[INFO] Remove svcrdeck as member from project '$PRO_ID'..."
      OS_PROJECT_NAME=admin OS_PROJECT_ID= openstack role remove --user svcrdeck --project $PRO_ID Member
    fi

  done

if [[ $K0 -ne 0 ]]; then
  echo "[ERROR] ai-remote-power-control failed for some hosts. Please check summary table"
  exit 1
fi
