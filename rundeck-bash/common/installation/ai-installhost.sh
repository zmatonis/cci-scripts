#!/bin/bash

source /var/lib/rundeck/data/openrc

KO=0

declare -A HOSTS

# remove .cern.ch and lowercase all hosts passed
HOSTS=`echo $RD_OPTION_HOSTS | sed 's/.cern.ch//g' | awk '{print tolower($0)}'`
echo "[INFO] Processing hosts $HOSTS"

for HOST in $HOSTS
do
    VM_ID=$(openstack server list --all-projects --name $HOST -n -c ID -f value)

    if [[ -n "${VM_ID}" ]]; then
        if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then
            PRO_ID=$(openstack server show $VM_ID -c project_id -f value)
            echo "[INFO] Rebuilding Ironic-managed host '$HOST' from project '$PRO_ID'"
            OS_PROJECT_NAME=admin OS_PROJECT_ID=        openstack role add    --user svcrdeck --project $PRO_ID Member
            OS_PROJECT_NAME=      OS_PROJECT_ID=$PRO_ID ai-rebuild-vm --cc7 $HOST
            OS_PROJECT_NAME=admin OS_PROJECT_ID=        openstack role remove --user svcrdeck --project $PRO_ID Member
        else
            echo "[DRYRUN] Running ai-rebuild in dryrun mode..."
        fi 
    else
        TODO+=" $HOST"
    fi

done

if [[ -n "${TODO}" ]]; then
    if [ ${RD_OPTION_BEHAVIOUR} == 'perform' ]; then

        echo "[INFO] Force partition table and medium for OpenStack Compute Nodes"
        ai-foreman updatehost --ptable 'OpenStack Compute Node' --medium 'CentOS mirror' $HOSTS
      
        echo "[INFO] Executing ai-installhost for host $HOSTS..."
        SUMMARY=$(ai-installhost --reboot --threads 4 --report-to $RD_JOB_USERNAME --roger-message "ai-installhost triggered by $RD_JOB_USERNAME using Rundeck ($RD_JOB_ID)" $HOSTS 2>&1)
        EXIT_CODE=$?
        echo "$SUMMARY"

        HOST_OK=$(echo "$SUMMARY" | grep -wF '| OK |' | awk '{print $2}' | wc -l)

        if [[ $EXIT_CODE -ne 0 ]] && [[ $HOST_OK -ne 0 ]]; then
          ((KO++))
        fi
    else
        echo "[DRYRUN] Running ai-installhost in dryrun mode..."
        ai-installhost --dryrun --reboot --report-to $RD_JOB_USERNAME --roger-message "ai-installhost triggered by $RD_JOB_USERNAME using Rundeck ($RD_JOB_ID)" $HOSTS
    fi
fi

if [[ $KO -ne 0 ]]; then
    echo "[ERROR] ai-rebuild and/or ai-installhost failed for some hosts. Please check summary table"
    exit 1
fi
