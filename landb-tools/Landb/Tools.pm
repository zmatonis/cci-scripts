package Landb::Tools;

use strict;
use Net::Netrc;
use Term::ReadKey;

sub get_credentials($);

sub get_credentials($){
    my $username = shift @_;
    my $mach = Net::Netrc->lookup("nice.cern.ch",$username);
    my $password = $mach->password if $mach;
    if (not defined $password){
        print STDOUT "Please give the password for the account $username: ";
        ReadMode("noecho",);
        chomp($password = <STDIN>);
        ReadMode("normal");
        print "\n";
        if (not defined $password){
            print STDERR "failed to read a passwd, exiting\n";
            exit 1;
        }
    }
    return $password;
}

1;
