import sys
import os
import urllib3
import socket
from landbclient.client import LanDB

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

landb_username = "vmmaster"
landb_password = os.environ["LANDB_PASSWORD"]
landb_hostname = "network.cern.ch"
landb_port = "443"
landb_protocol = "https"

landb = LanDB(landb_username, landb_password,
              landb_hostname, landb_port, version=6)

nodes = sys.argv[1:]

for node in nodes:
    delivery = node[0: 9]
    node_ip = socket.gethostbyname(node)
    device = landb.device_info(landb.device_search(ip_addr=node_ip)[0])
    for interface in device.Interfaces:
        if interface.Name == device.DeviceName + '.CERN.CH':
            svc_name = interface.ServiceName.replace('-', '_')
            resource_class = 'BAREMETAL_P1_{}_{}'.format(delivery.upper(), svc_name)
            ironic_command = "openstack baremetal node set {} --resource-class {}"
            print(ironic_command.format(node, resource_class))
