#!/bin/bash

NUM=42
CELL=gva_shared_018
FLAVOR=p1.dl7336805.S513-C-IP161
DELIVERY=7336805
IMAGE="CC7 - x86_64 mdraid [2018-08-27]"
KEY=<YOUR KEY GOES HERE>

for i in $(seq 1 $NUM); do
    host=`mkpasswd -l 7 -d 7 -c 0 -C 0 -s 0`
    echo ai-bs -f $FLAVOR --landb-mainuser ai-openstack-admin --landb-responsible ai-openstack-admin --foreman-environment nova_master_1 -g cloud_compute/level2/main/$CELL --nova-image $IMAGE --nova-sshkey $KEY --userdata-dir ./userdata i$DELIVERY$host
done
