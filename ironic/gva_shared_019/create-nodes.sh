#!/bin/bash

NUM=40
CELL=gva_shared_019
FLAVOR=p1.dl7336805.S513-C-IP162
DELIVERY=7336805

for i in $(seq 1 $NUM); do
    host=`mkpasswd -l 7 -d 7 -c 0 -C 0 -s 0`
    echo ai-bs -f $FLAVOR --landb-mainuser ai-openstack-admin --landb-responsible ai-openstack-admin --foreman-environment nova_master_2 -g cloud_compute/level2/main/$CELL --nova-image ac6c704c-7c07-4ec9-a9b3-81a8d71d0270 --nova-sshkey pcitfio23 --userdata-dir ./userdata i$DELIVERY$host
done
