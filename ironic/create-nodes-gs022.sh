NUM=$1
MAX=20   # Number of instances that can be created with this flavor
CELL=gva_shared_022
REGION=main
FLAVOR=p1.dl7872442.S513-C-IP851
DELIVERY=7872442
USERKEY=id_dsa
ENVIRONMENT=nova_prestage
# <-- end 'adapt these values'

export OS_PROJECT_NAME="Cloud Compute Nodes - ${CELL}"

CNT=`openstack server list --flavor ${FLAVOR} -f value -c Name | wc -w`
if (( ${CNT} + ${NUM} > ${MAX} ))
then
    echo Cannot create ${NUM} instances, there are already ${CNT} servers of a maximum ${MAX} servers in this project
    exit 1
fi

for i in $(seq 1 $NUM); do
    host=`mkpasswd -l 7 -d 7 -c 0 -C 0 -s 0`
    host="i${DELIVERY}${host}"
    echo $i - $host
    ai-bs -f $FLAVOR --landb-mainuser ai-openstack-admin --landb-responsible ai-openstack-admin \
          --foreman-environment $ENVIRONMENT -g cloud_compute/level2/$REGION/$CELL \
          --userdata-dir ./userdata \
          --nova-sshkey $USERKEY \
          --cc7 \
          $host
done

exit 
