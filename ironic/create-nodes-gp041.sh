#!/bin/bash

# make sure you're in the correct project before running this script

# adapt these values
NUM=$1
CELL=gva_project_041
FLAVOR=p1.dl6636494.S513-V-IP263
USERKEY=pcitfio23
REGION=batch
DELIVERY=6636494
# <-- end 'adapt these values'

for i in $(seq 1 $NUM); do
    host=`mkpasswd -l 7 -d 7 -c 0 -C 0 -s 0`
    echo $host
    echo ai-bs -f $FLAVOR --landb-mainuser ai-openstack-admin --landb-responsible ai-openstack-admin --foreman-environment nova_master_3 -g cloud_compute/level2/$REGION/$CELL --cc7 --nova-sshkey $USERKEY --userdata-dir ./userdata i$DELIVERY$host
done
