NUM=$1
MAX=20   # Number of instances that can be created with this flavor
CELL=gva_shared_021
REGION=main
FLAVOR=p1.dl7872442.S513-C-IP850
DELIVERY=7872442
USERKEY=id_dsa
ENVIRONMENT=nova_prestage
# <-- end 'adapt these values'

export OS_PROJECT_NAME="Cloud Compute Nodes - ${CELL}"

CNT=`openstack server list --flavor ${FLAVOR} -f value -c Name | wc -w`
if (( ${CNT} + ${NUM} > ${MAX} ))
then
    echo Cannot create ${NUM} instances, there are already ${CNT} servers of a maximum ${MAX} servers in this project
    exit 1
fi

for i in $(seq 1 $NUM); do
    host=`mkpasswd -l 7 -d 7 -c 0 -C 0 -s 0`
    host="i${DELIVERY}${host}"
    echo $i - $host
    ai-bs -f $FLAVOR --landb-mainuser ai-openstack-admin --landb-responsible ai-openstack-admin \
          --foreman-environment $ENVIRONMENT -g cloud_compute/level2/$REGION/$CELL \
          --userdata-dir ./userdata \
          --nova-sshkey $USERKEY \
          --cc7 \
          $host
done

exit 


+---------------------------------------+-------+----+
| Project                               | Cores | VM |
+---------------------------------------+-------+----+
| CMS Webtools Critical                 | 232   | 18 |
| CMS Webtools                          | 16    | 2  |
+---------------------------------------+-------+----+
| CMS iCMS                              | 48    | 8  |
| CMS Frontier                          | 44    | 8  |
| CMS online and offline databases      | 40    | 10 |
| CMS Computing Operations              | 34    | 11 |
| CMS PdmV                              | 18    | 8  |
| CMS Frontier LCG                      | 16    | 2  |
| CMS Web Based Monitoring              | 9     | 3  |
| CMS L1 Trigger software build service | 9     | 3  |
| CMS GEM DB                            | 8     | 2  |
| CMS CMSDCS WebCenter Development      | 8     | 1  |
| CMS Documentation Servers             | 8     | 1  |
| CMS Machine Learning Platforms        | 6     | 2  |
| CMS release validation                | 4     | 2  |
| CMS CRAB                              | 4     | 1  |
| CMS GlideinWMS                        | 4     | 1  |
| CMS Webtools                          | 4     | 1  |
| CMS Evolution                         | 2     | 1  |
| CMS SDT Build                         | 2     | 1  |
+---------------------------------------+-------+----+



