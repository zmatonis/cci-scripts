NUM=$1
CELL=pt8_project_004
REGION=point8
FLAVOR=p1.cd5795986.S6045-C6-IP104
DELIVERY=5795986
USERKEY=pcitfio23
# <-- end 'adapt these values'
for i in $(seq 1 $NUM); do
    host=`mkpasswd -l 7 -d 7 -c 0 -C 0 -s 0`
    echo $host
    ai-bs -f $FLAVOR --landb-mainuser ai-openstack-admin --landb-responsible ai-openstack-admin \
          --foreman-environment nova_master_1 -g cloud_compute/level2/$REGION/$CELL \
          --userdata-dir ./userdata \
          --nova-sshkey $USERKEY \
          --cc7 \
          i$DELIVERY$host
done

exit 
