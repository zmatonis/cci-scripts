#!/bin/bash

# make sure you're in the correct project before running this script

# adapt these values
NUM=$1
CELL=crit_project_004
FLAVOR=p1.dl7873990.S513-A-IP22
DELIVERY=7873990
# <-- end 'adapt these values'

for i in $(seq 1 $NUM); do
    host=`mkpasswd -l 7 -d 7 -c 0 -C 0 -s 0`
    echo $host
    ai-bs -f $FLAVOR --landb-mainuser ai-openstack-admin --landb-responsible ai-openstack-admin --foreman-environment nova_master_3 -g cloud_compute/level2/main/$CELL --cc7 i$DELIVERY$host
done
