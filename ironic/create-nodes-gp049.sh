NUM=$1
CELL=gva_project_049
REGION=batch
FLAVOR=p1.dl7881002.S513-V-IP116
DELIVERY=7881002
USERKEY=id_dsa
# <-- end 'adapt these values'
for i in $(seq 1 $NUM); do
    host=`mkpasswd -l 7 -d 7 -c 0 -C 0 -s 0`
    host="i${DELIVERY}${host}"
    echo $i - $host
    ai-bs -f $FLAVOR --landb-mainuser ai-openstack-admin --landb-responsible ai-openstack-admin \
          --foreman-environment nova_prestage -g cloud_compute/level2/$REGION/$CELL \
          --userdata-dir ./userdata \
          --nova-sshkey $USERKEY \
          --cc7 \
          $host
done

exit 
