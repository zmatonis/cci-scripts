#!/bin/bash

# make sure you're in the correct project before running this script

# adapt these values
NUM=$1
CELL=pt8_project_003
FLAVOR=p1.dl6253971.S6045_C6_IP103
DELIVERY=6253971
# <-- end 'adapt these values'

for i in $(seq 1 $NUM); do
    host=`mkpasswd -l 7 -d 7 -c 0 -C 0 -s 0`
    echo $host
    echo ai-bs -f $FLAVOR --landb-mainuser ai-openstack-admin --landb-responsible ai-openstack-admin --foreman-environment nova_master_3 -g cloud_compute/level2/point8/$CELL --cc7 --nova-sshkey pcitfio23 --userdata-dir ./userdata i$DELIVERY$host
done
