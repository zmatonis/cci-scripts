import logging
import os
import pymysql
import re
import sys

from pymysql.err import OperationalError

# configure logging
logging.basicConfig(level=logging.WARNING, format="%(message)s")
logger = logging.getLogger(__name__)

# Regular expresion for the connections in dblogger yaml file
regexp = (r'^\s*connection: \"mysql:\/\/%TEIGI__mysql_readonly'
          '_user__%:%TEIGI__mysql_readonly_password__%@'
          '([a-zA-Z0-9-\.]*):([0-9]*)\/([a-zA-Z0-9-\.\_]*)\"$')  # noqa: W605
filename = 'data/hostgroup/cloud_monitoring/dblogger.yaml'

username = 'dblogger'
password = os.environ['DBLOGGER_PASSWORD']

logger.warning('START checking the file...')
error = False

with open(filename, 'r') as stream:
    for line in stream.readlines():
        m = re.match(regexp, line)
        if m:
            host = m.group(1)
            port = int(m.group(2))
            dbname = m.group(3)
            try:
                connection = pymysql.connect(
                    host=host,
                    port=port,
                    user=username,
                    password=password,
                    db=dbname)
                connection.close()
                logger.info('SUCCESS to connect to %s:%s', host, str(port))
            except OperationalError as e:
                logger.error('FAILURE to connect to %s:%s', host, str(port))
                error = True

logger.warning('FINISH file validation')

if error:
    logger.error('STATUS There are failures while connecting with the databases')
    sys.exit(1)
else:
    logger.warning('STATUS OK')
